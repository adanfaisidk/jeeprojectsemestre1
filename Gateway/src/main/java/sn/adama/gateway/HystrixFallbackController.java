package sn.adama.gateway;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HystrixFallbackController {
    @RequestMapping("/utilisateurFallback")
    public Mono<String> utilisateurServiceFallback(){
        return Mono.just("Le service pour la gestion des utilisateurs prend du temps à répondre. Veuillez réessayer ultérieurement.");
    }
    @RequestMapping("/compteFallback")
    public Mono<String> compteServiceFallback(){
        return Mono.just("Le service pour la gestion des comptes utilisateurs prend du temps à répondre. Veuillez réessayer ultérieurement.");
    }
}
