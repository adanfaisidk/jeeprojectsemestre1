package sn.adama.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.adama.demo.entites.Compte;

@Repository
public interface CompteRepository extends JpaRepository<Compte, Long> {
}
