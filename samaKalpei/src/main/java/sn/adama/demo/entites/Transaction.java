package sn.adama.demo.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date transaction_date;
    private String numero;
    private String reference;
    private String type;
    private Double frais;
    private Double montant;
    private boolean status;
    //static belongsTo = [compteTo:Compte,accountFrom:Compte]
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "transaction_to_id")
//    private Compte transactionTo;
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "transaction_from_id")
//    private Compte transactionsFrom;



}
