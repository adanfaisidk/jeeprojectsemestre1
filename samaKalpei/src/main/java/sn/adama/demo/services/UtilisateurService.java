package sn.adama.demo.services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.adama.demo.entites.Utilisateur;
import sn.adama.demo.repository.UtilisateurRepository;

@Service
public class UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    public Utilisateur findByNom(String nom) {
        return utilisateurRepository.findByNom(nom);
    }
}
