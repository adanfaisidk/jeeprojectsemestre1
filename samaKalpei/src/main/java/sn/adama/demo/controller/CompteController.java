package sn.adama.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.adama.demo.entites.Compte;
import sn.adama.demo.entites.Utilisateur;
import sn.adama.demo.repository.CompteRepository;
import sn.adama.demo.repository.UtilisateurRepository;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CompteController {

    @Autowired
    private UtilisateurRepository userRepository;
    @Autowired
    private CompteRepository compteRepository;

    @PostMapping("/compte/add")
    public ResponseEntity<?> addUser(@RequestBody Compte compte) {
        Optional<Utilisateur> u = userRepository.findById(compte.getId_user());
        compte.setUtilisateur(u.get());
        compteRepository.save(compte);
        System.out.println(compte);
        return ResponseEntity.ok(compte);
    }


    @GetMapping("/compte/liste")
    public ResponseEntity<?> listeComptes() {
        return ResponseEntity.ok(compteRepository
                .findAll());
    }
}
