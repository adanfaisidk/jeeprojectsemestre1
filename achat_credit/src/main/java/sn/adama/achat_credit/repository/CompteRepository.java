package sn.adama.achat_credit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.adama.achat_credit.entites.Compte;

@Repository
public interface CompteRepository extends JpaRepository<Compte, Long> {
}
